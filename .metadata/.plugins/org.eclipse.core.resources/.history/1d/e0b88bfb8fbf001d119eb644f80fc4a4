/*------------------------------------------------------------------------------
STANDAR:
------------------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_adc.h"
#include "stdio.h"
#include "stm32f4xx_tim.h"
#include "math.h"
#include "misc.h"

/*Declaracion de estructuras:*/
TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
TIM_OCInitTypeDef       TIM_OCInitStructure;
EXTI_InitTypeDef        EXTI_InitStructure;
ADC_InitTypeDef         ADC_InitStructure;
ADC_CommonInitTypeDef   ADC_CommonInitStructure;
DMA_InitTypeDef         DMA_InitStructure;
NVIC_InitTypeDef        NVIC_InitStructure;
GPIO_InitTypeDef        GPIO_InitStructure;

/*Declaracion de funciones base:*/
void INIT_DO(GPIO_TypeDef* Port, uint32_t Pin);
uint32_t FIND_CLOCK(GPIO_TypeDef *Port);
uint8_t FIND_PINSOURCE(uint32_t Pin);
void INIT_ALL(void);
uint8_t ack = 0;
void ACK(void);

char tempBuff[10];

#define RCV_BUFFER_SIZE 10

/*------------------------------------------------------------------------------
LCD:
------------------------------------------------------------------------------*/
/* * * * * * * * * * *          DEFINES        * * * * * * * * * * * * * * */
#define  TLCD_INIT_PAUSE  100000  // pause beim init (>=70000)
#define  TLCD_PAUSE        50000  // kleine Pause (>=20000)
#define  TLCD_CLK_PAUSE     1000  // pause for Clock-Impuls (>=500)
#define  TLCD_MAXX            16  // max x-Position (0...15)
#define  TLCD_MAXY             2  // max y-Position (0...1)
#define  DAC_DHR12R2_ADDRESS   0x40007414
#define  TLCD_CMD_INIT_DISPLAY  0x28   // 2 Zeilen Display, 5x7 Punkte
#define  TLCD_CMD_ENTRY_MODE    0x06   // Cursor increase, Display fix
#define  TLCD_CMD_DISP_M0       0x08   // Display=AUS, Cursor=Aus, Blinken=Aus
#define  TLCD_CMD_DISP_M1       0x0C   // Display=EIN, Cursor=AUS, Blinken=Aus
#define  TLCD_CMD_DISP_M2       0x0E   // Display=EIN, Cursor=EIN, Blinken=Aus
#define  TLCD_CMD_DISP_M3       0x0F   // Display=EIN, Cursor=EIN, Blinken=EIN
#define  TLCD_CMD_CLEAR         0x01   // loescht das Display

typedef enum
{
  TLCD_RS = 0,  // RS-Pin
  TLCD_E  = 1,  // E-Pin
  TLCD_D4 = 2,  // DB4-Pin
  TLCD_D5 = 3,  // DB5-Pin
  TLCD_D6 = 4,  // DB6-Pin
  TLCD_D7 = 5   // DB7-Pin
}TLCD_NAME_t;

#define  TLCD_ANZ   6 // Anzahl von TLCD_NAME_t

typedef enum {
  TLCD_OFF = 0,    // Display=AUS, Cursor=Aus, Blinken=Aus
  TLCD_ON,         // Display=EIN, Cursor=Aus, Blinken=Aus
  TLCD_CURSOR,     // Display=EIN, Cursor=EIN, Blinken=Aus
  TLCD_BLINK       // Display=EIN, Cursor=EIN, Blinken=EIN
}TLCD_MODE_t;

typedef struct {
  TLCD_NAME_t TLCD_NAME;   // Name
  GPIO_TypeDef* TLCD_PORT; // Port
  const uint16_t TLCD_PIN; // Pin
  const uint32_t TLCD_CLK; // Clock
  BitAction TLCD_INIT;     // Init
}LCD_2X16_t;

/* * * * * * * * * * *          DEFINES        * * * * * * * * * * * * * * */
/*Longitud general de buffers:*/
#define buffLen 20

/*Agotamiento de cuenta del TIM3 para refrescar LCD:*/
#define freqTIM3 4

/* * * * * * * * * * *         VARIABLES       * * * * * * * * * * * * * * */
/*Pantalla inicial:*/
uint8_t initialScreen = 1;

/*Contador de 5 segundos:*/
uint8_t fiveSecDelay = 0;

/*Buffers para mostrar las variables:*/
char buffTemp1[8];
char buffTemp2[8];
char buffTemp [8];
char temp1[8];
char temp2[8];
char temp3[8];
uint8_t d = 0;
uint8_t delay = 0;
uint8_t flagTemp2 = 0;
uint8_t flagTemp3 = 0;

/*Definicion de pines del LCD:*/
LCD_2X16_t LCD_2X16[] = {
    /* Name  , PORT ,     PIN    ,         CLOCK       ,   Init    */
    { TLCD_RS, GPIOD, GPIO_Pin_7,  RCC_AHB1Periph_GPIOD, Bit_RESET },
    { TLCD_E,  GPIOE, GPIO_Pin_2,  RCC_AHB1Periph_GPIOE, Bit_RESET },
    { TLCD_D4, GPIOE, GPIO_Pin_4,  RCC_AHB1Periph_GPIOE, Bit_RESET },
    { TLCD_D5, GPIOE, GPIO_Pin_5,  RCC_AHB1Periph_GPIOE, Bit_RESET },
    { TLCD_D6, GPIOE, GPIO_Pin_6,  RCC_AHB1Periph_GPIOE, Bit_RESET },
    { TLCD_D7, GPIOE, GPIO_Pin_3,  RCC_AHB1Periph_GPIOE, Bit_RESET },};

/* * * * * * * * * * *         FUNCIONES       * * * * * * * * * * * * * * */
/*Refrescar datos LCD:*/
void LCD(void);

/*Funciones generales LCD:*/
void INIT_LCD_2x16(LCD_2X16_t*);
void CLEAR_LCD_2x16(LCD_2X16_t*);
void PRINT_LCD_2x16(LCD_2X16_t*, uint8_t, uint8_t, char*);
void P_LCD_2x16_InitIO(LCD_2X16_t *LCD_2X16);
void P_LCD_2x16_PinLo(TLCD_NAME_t lcd_pin, LCD_2X16_t *LCD_2X16);
void P_LCD_2x16_PinHi(TLCD_NAME_t lcd_pin, LCD_2X16_t *LCD_2X16);
void P_LCD_2x16_Delay(volatile uint32_t nCount);
void P_LCD_2x16_InitSequenz(LCD_2X16_t *LCD_2X16);
void P_LCD_2x16_Clk(LCD_2X16_t *LCD_2X16);
void P_LCD_2x16_Cmd(uint8_t wert, LCD_2X16_t *LCD_2X16);
void P_LCD_2x16_Cursor(LCD_2X16_t *LCD_2X16, uint8_t x, uint8_t y);
void P_LCD_2x16_Data(uint8_t wert, LCD_2X16_t *LCD_2X16);

/*----------------------------------------------------------------*/
/*HARDWARE:                                                       */
/*----------------------------------------------------------------*/
/*Pulsadores:*/
#define _F1 GPIOC
#define _f1 GPIO_Pin_6
#define _F2 GPIOC
#define _f2 GPIO_Pin_7
#define _C1 GPIOB
#define _c1 GPIO_Pin_8
#define _C2 GPIOC
#define _c2 GPIO_Pin_9


/*----------------------------------------------------------------*/
/*USART:                                                          */
/*----------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include <string.h>
#include "stm32f4xx_usart.h"

int ready = 0 ;

void GPIO_Configuration(void)
{
	GPIO_InitTypeDef 	GPIO_InitStruct;

	// Enable clock for GPIOA
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);//verrr

	/**
	* Tell pins PA9 and PA10 which alternating function you will use
	* @important Make sure, these lines are before pins configuration!
	*/
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
	// Initialize pins as alternating function
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

}

void USART2_Configuration(void)
{
	USART_InitTypeDef USART_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;

	/**
	 * Enable clock for USART1 peripheral
	 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	/**
	 * Set Baudrate to value you pass to function
	 * Disable Hardware Flow control
	 * Set Mode To TX and RX, so USART will work in full-duplex mode
	 * Disable parity bit
	 * Set 1 stop bit
	 * Set Data bits to 8
	 *
	 * Initialize USART1
	 * Activate USART1
	 */
	USART_InitStruct.USART_BaudRate = 9600;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART1, &USART_InitStruct);
	USART_Cmd(USART1, ENABLE);

	/**
	 * Enable RX interrupt
	 */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

	/**
	 * Set Channel to USART1
	 * Set Channel Cmd to enable. That will enable USART1 channel in NVIC
	 * Set Both priorities to 0. This means high priority
	 *
	 * Initialize NVIC
	 */
	NVIC_InitStruct.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 2;
	NVIC_Init(&NVIC_InitStruct);

}

//para leer
uint8_t CE_read_word_BT( char *buffer, int tam) {

	static int pos = 0;
	char caracter = USART_ReceiveData(USART1);

	if (caracter == '\n' || caracter == '\r') {
		pos = 0;
		return 0;
	} else {
		buffer[pos] = caracter;
		pos++;
	}

	if (pos >= tam) {
		pos = 0;
		return 0;
	}
	return 1;
}
//para escribir
void CE_send_BT( char *content) {
	int i;
	for (i = 0; i < strlen(content); i++) {

		char dato = content[i];
		USART1->DR = dato;
		//USART_SendData(USART1,dato);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
	}
}

/*----------------------------------------------------------------*/
/*EXTI:                                                           */
/*----------------------------------------------------------------*/
/* * * * * * * * * * *         VARIABLES       * * * * * * * * * * * * * * */
/*Switch temperatura:*/
uint8_t switchTemp   = 0;
uint8_t switchConn  = 0;

/*Switch SD:*/
uint8_t switchServo1 = 0;

/*Switch servo:*/
uint8_t switchServo2 = 0;

/*Switch extra:*/
uint8_t switchSD     = 0;

/*Control de switch activo:*/
uint8_t activeSwitch = 0;

/* * * * * * * * * * *         FUNCIONES       * * * * * * * * * * * * * * */
/*Configuracion de nterrupciones externas:*/
void INIT_EXTINT(GPIO_TypeDef* Port, uint16_t Pin);
uint8_t FIND_EXTI_PORT_SOURCE(GPIO_TypeDef* Port);
uint8_t FIND_EXTI_PIN_SOURCE(uint32_t Pin);
uint32_t FIND_EXTI_LINE(uint32_t Pin);
uint32_t FIND_EXTI_HANDLER(uint32_t Pin);

/*----------------------------------------------------------------*/
/*TIMERS:                                                           */
/*----------------------------------------------------------------*/
void INIT_TIM3(uint32_t Freq);

/*----------------------------------------------------------------*/
/*MENUS:                                                           */
/*----------------------------------------------------------------*/
void INITIAL_SCREEN(void);
void UPDATE_TEMP(void);
void MOVE_SERVO_1(void);
void MOVE_SERVO_2(void);
void SD(void);

/*----------------------------------------------------------------*/
/*LOCAL:                                                           */
/*----------------------------------------------------------------*/
void TEMP_CODE(void);
void SERVO_1(void);
void SERVO_2(void);
void SD(void);
void SD_CODE(void);

#define TM_USART2_BUFFER_SIZE 23
char rcvBuffer[TM_USART2_BUFFER_SIZE];
char rcvMessage[TM_USART2_BUFFER_SIZE];
uint32_t rcvIndestaticx = 0;
uint32_t rcvMessageSize = 0;

//int main(void);

/*----------------------------------------------------------------*/
/*MAIN:                                                           */
/*----------------------------------------------------------------*/
int main(void){

	/* Initialize system */
	SystemInit();

	/*Inicializacion de puertos y funciones del sistema:*/
	INIT_ALL();
	GPIO_Configuration();
	USART2_Configuration();

/* * * * * * * * * * * * * BUCLE PPAL. * * * * * * * * * * * * */
  while (1)
  {

  }
}

/*----------------------------------------------------------------*/
/*INTERRUPCIONES:                                                 */
/*----------------------------------------------------------------*/

//void USART1_IRQHandler(void)
//{
//	if(USART_GetITStatus(USART1,USART_IT_RXNE)==SET)
//	{
//		if(USART_GetFlagStatus(USART1,USART_IT_RXNE)==SET)
//		{
//			rcvBuffer[rcvIndex]=(char) USART_ReceiveData(USART1);
//
//			//Buffer full or end of current message
//			if(rcvIndex+1>=RCV_BUFFER_SIZE || rcvBuffer[rcvIndex]=='\n')
//			{
//				//Placing message in message buffer - setting flag
//
//				uint32_t i;
//				for(i=0;i<RCV_BUFFER_SIZE;i++)
//				{
//					if(i<=rcvIndex)
//					{
//						rcvMessage[i]=rcvBuffer[i];
//					}
//					else
//					{
//						rcvMessage[i]=0x00;
//					}
//				}
//
////				CE_read_word_BT(c,10);
//
//				//Clear receive buffer
//				rcvMessageSize=rcvIndex;
//				//processMessageRecieved();
//				for(i=0;i<RCV_BUFFER_SIZE;i++)
//				{
//					rcvBuffer[i]=0x00;
//				}
//
//				rcvIndex=0;
//			}
//			else
//			{
//				rcvIndex++;
//			}
//		}
//		USART_ClearITPendingBit(USART1,USART_IT_RXNE);
//	}
//}

void USART1_IRQHandler(void) {
//		CE_read_word_BT(rcvMessage,10);
	static int pos = 0;
	char caracter = USART_ReceiveData(USART1);

	if (caracter == '\n' || caracter == '\r') {
		pos = 0;
	} else {
		rcvMessage[pos] = caracter;
		pos++;
	}
	if (pos >= 10) {
		pos = 0;
		return 0;
	}
}

/*Interrupcion por vencimiento de cuenta de TIM3 cada 1/FS:*/
void TIM3_IRQHandler(void) {
  if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) {
    /*Refresco del LCD:*/
    LCD();

    /*Rehabilitacion del timer:*/
    TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
  }
}

/*Interrupcion al pulso por PC6-C1 o PC8-C2:*/
void EXTI9_5_IRQHandler(void)
{
	/*Si la interrupcion fue por linea 6 (PC6 - C1):*/
	if (EXTI_GetITStatus(EXTI_Line6) != RESET) {
		CE_send_BT("a\n");
		switchSD = 1;
	}
//
//	/*Si la interrupcion fue por linea 8 (PC8 - C2):*/
//	else if (EXTI_GetITStatus(EXTI_Line7) != RESET) {
//	}
//
	/*Si la interrupcion fue por linea 8 (PC8 - C2):*/
	else if (EXTI_GetITStatus(EXTI_Line8) != RESET) {
		CE_send_BT("b\n");
		switchServo2 = 1;
	}

	/*Si la interrupcion fue por linea 8 (PC8 - C2):*/
	else if (EXTI_GetITStatus(EXTI_Line9) != RESET) {
		CE_send_BT("c\n");
		switchServo1 = 1;
	}
	else
		initialScreen = 1;

  EXTI_ClearITPendingBit(EXTI_Line6);
  EXTI_ClearFlag(EXTI_Line6);
  EXTI_ClearITPendingBit(EXTI_Line7);
  EXTI_ClearFlag(EXTI_Line7);
//  EXTI_ClearITPendingBit(EXTI_Line8);
//  EXTI_ClearFlag(EXTI_Line8);
  EXTI_ClearITPendingBit(EXTI_Line9);
  EXTI_ClearFlag(EXTI_Line9);


  /*Evitar que se muestre la pantalla incial:*/
  initialScreen = 0;
}

/*----------------------------------------------------------------*/
/*LCD:                                                            */
/*----------------------------------------------------------------*/
void LCD(void)
{
    /*Refresco LCD:*/
    CLEAR_LCD_2x16(LCD_2X16);

    /*Pantalla incial:*/
    if(initialScreen == 1){
        INITIAL_SCREEN();
    }

    /*Pantalla actualizar temperatura - pulsador S1:*/
    else if(switchTemp == 1 && fiveSecDelay <= 10){
        UPDATE_TEMP();
    }
    /*Pantalla SERVO posicion 2- pulsador S2:*/
    else if(switchServo1 == 1 && fiveSecDelay <= 10){
        MOVE_SERVO_2();
    }

    /*Pantalla mover servo posicion 1 - pulsador S3:*/
    else if(switchServo2 == 1 && fiveSecDelay <= 10){
        MOVE_SERVO_1();
    }

    /*Pantalla datos guardados SD - pulsador S4:*/
    else if(switchSD == 1 && fiveSecDelay <= 10){
        SD();
    }

    /*Reseteo variables:*/
    else {
        initialScreen   = 1;
        fiveSecDelay    = 0;
    }

    /*Actualizar estado teclado matricial:*/
    //UPDATE_SWITCH();

    /*Rehabilitacion del timer:*/
    TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
}

/*Pantalla inicial:*/
void INITIAL_SCREEN(void)
{
    /*Algoritmo para mostrar el mensaje:*/
    PRINT_LCD_2x16(LCD_2X16, 2, 0, "TD II-ERDYTC");
    PRINT_LCD_2x16(LCD_2X16, 0, 1, "  T = ");
    PRINT_LCD_2x16(LCD_2X16, 7, 1, rcvMessage);
    PRINT_LCD_2x16(LCD_2X16, 12, 1, " C   ");

//	for(uint8_t i=0;i<RCV_BUFFER_SIZE;i++)
//	{
//		rcvMessage[i]=0x00;
//	}
}

/*Pantalla para mostrar mensaje de actualizacion de temperatura:*/
void UPDATE_TEMP(void)
{
    /*Aumentar el contador de los 5 seg:*/
    fiveSecDelay++;

    /*Algoritmo para mostrar el mensaje:*/
    PRINT_LCD_2x16(LCD_2X16, 2, 0, "NO HAY");
    PRINT_LCD_2x16(LCD_2X16, 2, 1, " CONEXION");

    /*Pedir el valor de temperatura si pasaron 5 segundos:*/
    if (fiveSecDelay == 10)
        TEMP_CODE();
}

/*Pantalla para indicar que el servo se mueve a posicion 2:*/
void MOVE_SERVO_2(void)
{
    /*Aumentar el contador de los 5 seg:*/
    fiveSecDelay++;

    /*Algoritmo para mostrar el mensaje:*/
    PRINT_LCD_2x16(LCD_2X16, 1, 0, "SERVO POSICION");
    PRINT_LCD_2x16(LCD_2X16, 8, 1, "2");

    /*Guardar datos en SD si pasaron 5 segundos:*/
    if (fiveSecDelay == 10)
        SERVO_2();
}

/*Pantalla para indicar que el servo se mueve a posicion 1:*/
void MOVE_SERVO_1(void)
{
    /*Aumentar el contador de los 5 seg:*/
    fiveSecDelay++;

    /*Algoritmo para mostrar el mensaje:*/
    PRINT_LCD_2x16(LCD_2X16, 1, 0, "SERVO POSICION");
    PRINT_LCD_2x16(LCD_2X16, 8, 1, "1");

    /*Enviar comando servo si pasaron 5 segundos:*/
    if (fiveSecDelay == 10)
        SERVO_1();
}

/*Pantalla datos guardados SD:*/
void SD(void)
{
    /*Aumentar el contador de los 5 seg:*/
    fiveSecDelay++;

    /*Algoritmo para mostrar el mensaje:*/
    PRINT_LCD_2x16(LCD_2X16, 1, 0, "DATOS GUARDADOS");
    PRINT_LCD_2x16(LCD_2X16, 5, 1, "EN SD");

    /*Enviar comando servo si pasaron 5 segundos:*/
    if (fiveSecDelay == 10)
        SD_CODE();
}

void TEMP_CODE(void){
    /*Resetear flag del switch:*/
    switchTemp = 0;

    /*Iniciar la transmision:*/
//    CE_send_BT("a\n");

    NVIC_SystemReset();
}

void SERVO_1(void){
    /*Resetear flag switch:*/
    switchServo1 = 0;

    /*Iniciar la transmision:*/
//    CE_send_BT("b\n");

//    NVIC_SystemReset();
}

void SERVO_2(void){
  /*Resetear flag switch:*/
  switchServo2 = 0;

  /*Iniciar la transmision:*/
//  CE_send_BT("c\n");

//  NVIC_SystemReset();
}

void SD_CODE(void){
	  /*Resetear flag switch:*/
	  switchSD = 0;

	  /*Iniciar la transmision:*/
	//  CE_send_BT("c\n");

//	  NVIC_SystemReset();
}

/*----------------------------------------------------------------*/
/*INTERNAS LCD:                                                   */
/*----------------------------------------------------------------*/
/*Inicializacion LCD:*/
void INIT_LCD_2x16(LCD_2X16_t *LCD_2X16) {
  // Inicialización de los pines del LCD:
  P_LCD_2x16_InitIO(LCD_2X16);
  // kleine Pause
  P_LCD_2x16_Delay(TLCD_INIT_PAUSE);
  // Init Sequenz starten
  P_LCD_2x16_InitSequenz(LCD_2X16);
  // LCD-Settings einstellen
  P_LCD_2x16_Cmd(TLCD_CMD_INIT_DISPLAY, LCD_2X16);
  P_LCD_2x16_Cmd(TLCD_CMD_ENTRY_MODE, LCD_2X16);
  // Display einschalten
  P_LCD_2x16_Cmd(TLCD_CMD_DISP_M1, LCD_2X16);
  // Display l�schen
  P_LCD_2x16_Cmd(TLCD_CMD_CLEAR, LCD_2X16);
  // kleine Pause
  P_LCD_2x16_Delay(TLCD_PAUSE);
}

/*Refresco LCD:*/
void CLEAR_LCD_2x16(LCD_2X16_t *LCD_2X16) {
  // Display l�schen
  P_LCD_2x16_Cmd(TLCD_CMD_CLEAR, LCD_2X16);
  // kleine Pause
  P_LCD_2x16_Delay(TLCD_PAUSE);
}

/*Imprimir en pantalla LCD:*/
void PRINT_LCD_2x16(LCD_2X16_t *LCD_2X16, uint8_t x, uint8_t y, char *ptr) {
  // Cursor setzen
  P_LCD_2x16_Cursor(LCD_2X16, x, y);
  // kompletten String ausgeben
  while (*ptr != 0) {
    P_LCD_2x16_Data(*ptr, LCD_2X16);
    ptr++;
  }
}

// LCD:
void P_LCD_2x16_InitIO(LCD_2X16_t *LCD_2X16) {
  GPIO_InitTypeDef GPIO_InitStructure;
  TLCD_NAME_t lcd_pin;

  for (lcd_pin = 0; lcd_pin < TLCD_ANZ; lcd_pin++) {
    // Habilitacion del Clock para cada PIN:
    RCC_AHB1PeriphClockCmd(LCD_2X16[lcd_pin].TLCD_CLK, ENABLE);

    // Configuracion como salidas digitales:
    GPIO_InitStructure.GPIO_Pin = LCD_2X16[lcd_pin].TLCD_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(LCD_2X16[lcd_pin].TLCD_PORT, &GPIO_InitStructure);

    // Default Wert einstellen
    if (LCD_2X16[lcd_pin].TLCD_INIT == Bit_RESET)
      P_LCD_2x16_PinLo(lcd_pin, LCD_2X16);
    else
      P_LCD_2x16_PinHi(lcd_pin, LCD_2X16);
  }
}

void P_LCD_2x16_PinLo(TLCD_NAME_t lcd_pin, LCD_2X16_t *LCD_2X16) {
  LCD_2X16[lcd_pin].TLCD_PORT->BSRRH = LCD_2X16[lcd_pin].TLCD_PIN;
}

void P_LCD_2x16_PinHi(TLCD_NAME_t lcd_pin, LCD_2X16_t *LCD_2X16) {
  LCD_2X16[lcd_pin].TLCD_PORT->BSRRL = LCD_2X16[lcd_pin].TLCD_PIN;
}

void P_LCD_2x16_Delay(volatile uint32_t nCount) {
  while (nCount--) {
  }
}

void P_LCD_2x16_InitSequenz(LCD_2X16_t *LCD_2X16) {
  // Inicializacion de la secuencia:
  P_LCD_2x16_PinHi(TLCD_D4, LCD_2X16);
  P_LCD_2x16_PinHi(TLCD_D5, LCD_2X16);
  P_LCD_2x16_PinLo(TLCD_D6, LCD_2X16);
  P_LCD_2x16_PinLo(TLCD_D7, LCD_2X16);
  // Erster Init Impuls
  P_LCD_2x16_Clk(LCD_2X16);
  P_LCD_2x16_Delay(TLCD_PAUSE);
  // Zweiter Init Impuls
  P_LCD_2x16_Clk(LCD_2X16);
  P_LCD_2x16_Delay(TLCD_PAUSE);
  // Dritter Init Impuls
  P_LCD_2x16_Clk(LCD_2X16);
  P_LCD_2x16_Delay(TLCD_PAUSE);
  // LCD-Modus einstellen (4Bit-Mode)
  P_LCD_2x16_PinLo(TLCD_D4, LCD_2X16);
  P_LCD_2x16_PinHi(TLCD_D5, LCD_2X16);
  P_LCD_2x16_PinLo(TLCD_D6, LCD_2X16);
  P_LCD_2x16_PinLo(TLCD_D7, LCD_2X16);
  P_LCD_2x16_Clk(LCD_2X16);
  P_LCD_2x16_Delay(TLCD_PAUSE);
}

void P_LCD_2x16_Clk(LCD_2X16_t *LCD_2X16) {
  // Pin-E auf Hi
  P_LCD_2x16_PinHi(TLCD_E, LCD_2X16);
  // kleine Pause
  P_LCD_2x16_Delay(TLCD_CLK_PAUSE);
  // Pin-E auf Lo
  P_LCD_2x16_PinLo(TLCD_E, LCD_2X16);
  // kleine Pause
  P_LCD_2x16_Delay(TLCD_CLK_PAUSE);
}

void P_LCD_2x16_Cmd(uint8_t wert, LCD_2X16_t *LCD_2X16) {
  // RS=Lo (Command)
  P_LCD_2x16_PinLo(TLCD_RS, LCD_2X16);
  // Hi-Nibble ausgeben
  if ((wert & 0x80) != 0)
    P_LCD_2x16_PinHi(TLCD_D7, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D7, LCD_2X16);
  if ((wert & 0x40) != 0)
    P_LCD_2x16_PinHi(TLCD_D6, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D6, LCD_2X16);
  if ((wert & 0x20) != 0)
    P_LCD_2x16_PinHi(TLCD_D5, LCD_2X16);
  else P_LCD_2x16_PinLo(TLCD_D5, LCD_2X16);
  if ((wert & 0x10) != 0)
    P_LCD_2x16_PinHi(TLCD_D4, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D4, LCD_2X16);
  P_LCD_2x16_Clk(LCD_2X16);
  // Lo-Nibble ausgeben
  if ((wert & 0x08) != 0)
    P_LCD_2x16_PinHi(TLCD_D7, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D7, LCD_2X16);
  if ((wert & 0x04) != 0)
    P_LCD_2x16_PinHi(TLCD_D6, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D6, LCD_2X16);
  if ((wert & 0x02) != 0)
    P_LCD_2x16_PinHi(TLCD_D5, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D5, LCD_2X16);
  if ((wert & 0x01) != 0)
    P_LCD_2x16_PinHi(TLCD_D4, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D4, LCD_2X16);
  P_LCD_2x16_Clk(LCD_2X16);
}

void P_LCD_2x16_Cursor(LCD_2X16_t *LCD_2X16, uint8_t x, uint8_t y) {
  uint8_t wert;

  if (x >= TLCD_MAXX)
    x = 0;
  if (y >= TLCD_MAXY)
    y = 0;

  wert = (y << 6);
  wert |= x;
  wert |= 0x80;
  P_LCD_2x16_Cmd(wert, LCD_2X16);
}

void P_LCD_2x16_Data(uint8_t wert, LCD_2X16_t *LCD_2X16) {
  // RS=Hi (Data)
  P_LCD_2x16_PinHi(TLCD_RS, LCD_2X16);
  // Hi-Nibble ausgeben
  if ((wert & 0x80) != 0)
    P_LCD_2x16_PinHi(TLCD_D7, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D7, LCD_2X16);
  if ((wert & 0x40) != 0)
    P_LCD_2x16_PinHi(TLCD_D6, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D6, LCD_2X16);
  if ((wert & 0x20) != 0)
    P_LCD_2x16_PinHi(TLCD_D5, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D5, LCD_2X16);
  if ((wert & 0x10) != 0)
    P_LCD_2x16_PinHi(TLCD_D4, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D4, LCD_2X16);
  P_LCD_2x16_Clk(LCD_2X16);
  // Lo-Nibble ausgeben
  if ((wert & 0x08) != 0)
    P_LCD_2x16_PinHi(TLCD_D7, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D7, LCD_2X16);
  if ((wert & 0x04) != 0)
    P_LCD_2x16_PinHi(TLCD_D6, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D6, LCD_2X16);
  if ((wert & 0x02) != 0)
    P_LCD_2x16_PinHi(TLCD_D5, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D5, LCD_2X16);
  if ((wert & 0x01) != 0)
    P_LCD_2x16_PinHi(TLCD_D4, LCD_2X16);
  else
    P_LCD_2x16_PinLo(TLCD_D4, LCD_2X16);
  P_LCD_2x16_Clk(LCD_2X16);
}

/*----------------------------------------------------------------*/
/*TIMERS:                                                         */
/*----------------------------------------------------------------*/
/*Inicializacion TIM3:*/
void INIT_TIM3(uint32_t Freq) {

    /*Habilitacion del clock para el TIM3:*/
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

    /*Habilitacion de la interrupcion por agotamiento de cuenta del TIM3:*/
    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /*Actualización de los valores del TIM3:*/
    SystemCoreClockUpdate();
    TIM_ITConfig(TIM3, TIM_IT_Update, DISABLE);
    TIM_Cmd(TIM3, DISABLE);

    /*Definicion de la base de tiempo:*/
    uint32_t TimeBase = 200e3;

    /*Computar el valor del preescaler en base a la base de tiempo:*/
    uint16_t PrescalerValue = 0;
    PrescalerValue = (uint16_t) ((SystemCoreClock / 2) / TimeBase) - 1;

    /*Configuracion del tiempo de trabajo en base a la frecuencia:*/
    TIM_TimeBaseStructure.TIM_Period = TimeBase / Freq - 1;
    TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

    /*Habilitacion de la interrupcion:*/
    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

    /*Habilitacion del contador:*/
    TIM_Cmd(TIM3, ENABLE);
}

/*----------------------------------------------------------------*/
/*STANDAR:                                                        */
/*----------------------------------------------------------------*/
void INIT_DO(GPIO_TypeDef *Port, uint32_t Pin) {
  // Estructura de configuracion
  GPIO_InitTypeDef GPIO_InitStructure;

  // Habilitacion de la senal de reloj para el periferico:
  uint32_t Clock;
  Clock = FIND_CLOCK(Port);
  RCC_AHB1PeriphClockCmd(Clock, ENABLE);
  // Se configura el pin como entrada (GPI0_MODE_IN):
  GPIO_InitStructure.GPIO_Pin = Pin;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

  // Se aplica la configuracion definida anteriormente al puerto:
  GPIO_Init(Port, &GPIO_InitStructure);
}

/*Encontrar clock de determinado pin:*/
uint32_t FIND_CLOCK(GPIO_TypeDef* Port)
{
    uint32_t Clock;

    if      (Port == GPIOA) Clock = RCC_AHB1Periph_GPIOA;
    else if (Port == GPIOB) Clock = RCC_AHB1Periph_GPIOB;
    else if (Port == GPIOC) Clock = RCC_AHB1Periph_GPIOC;
    else if (Port == GPIOD) Clock = RCC_AHB1Periph_GPIOD;
    else if (Port == GPIOE) Clock = RCC_AHB1Periph_GPIOE;
    else if (Port == GPIOF) Clock = RCC_AHB1Periph_GPIOF;
    else if (Port == GPIOG) Clock = RCC_AHB1Periph_GPIOG;
    return Clock;
}

/*Encontrar PinSource de determinado pin:*/
uint8_t FIND_PINSOURCE(uint32_t Pin)
{
    if     (Pin == GPIO_Pin_0)  return GPIO_PinSource0;
    else if(Pin == GPIO_Pin_1)  return GPIO_PinSource1;
    else if(Pin == GPIO_Pin_2)  return GPIO_PinSource2;
    else if(Pin == GPIO_Pin_3)  return GPIO_PinSource3;
    else if(Pin == GPIO_Pin_4)  return GPIO_PinSource4;
    else if(Pin == GPIO_Pin_5)  return GPIO_PinSource5;
    else if(Pin == GPIO_Pin_6)  return GPIO_PinSource6;
    else if(Pin == GPIO_Pin_7)  return GPIO_PinSource7;
    else if(Pin == GPIO_Pin_8)  return GPIO_PinSource8;
    else if(Pin == GPIO_Pin_9)  return GPIO_PinSource9;
    else if(Pin == GPIO_Pin_10) return GPIO_PinSource10;
    else if(Pin == GPIO_Pin_11) return GPIO_PinSource11;
    else if(Pin == GPIO_Pin_12) return GPIO_PinSource12;
    else if(Pin == GPIO_Pin_13) return GPIO_PinSource13;
    else if(Pin == GPIO_Pin_14) return GPIO_PinSource14;
    else if(Pin == GPIO_Pin_15) return GPIO_PinSource15;
    else
         return 0;
}


/*----------------------------------------------------------------*/
/*EXTI:                                                           */
/*----------------------------------------------------------------*/
/*Inicializacion de puertos EXTI:*/
void INIT_EXTINT(GPIO_TypeDef* Port, uint16_t Pin)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    /*Enable GPIO clock:*/
    uint32_t Clock;
    Clock = FIND_CLOCK(Port);
    RCC_AHB1PeriphClockCmd(Clock, ENABLE);
    /* Enable SYSCFG clock */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    /* Configure pin as input floating */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Pin = Pin;
    GPIO_Init(Port, &GPIO_InitStructure);

    /* Connect EXTI Line to pin */
    SYSCFG_EXTILineConfig(FIND_EXTI_PORT_SOURCE(Port), FIND_EXTI_PIN_SOURCE(Pin));

    /* Configure EXTI Line0 */
    EXTI_InitStructure.EXTI_Line = FIND_EXTI_LINE(Pin);
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    /* Enable and set EXTI Line0 Interrupt to the lowest priority */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    NVIC_InitStructure.NVIC_IRQChannel = FIND_EXTI_HANDLER(Pin);
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

uint8_t FIND_EXTI_PORT_SOURCE(GPIO_TypeDef* Port)
{
    if (Port == GPIOA)      return EXTI_PortSourceGPIOA;
    else if (Port == GPIOB) return EXTI_PortSourceGPIOB;
    else if (Port == GPIOC) return EXTI_PortSourceGPIOC;
    else if (Port == GPIOD) return EXTI_PortSourceGPIOD;
    else if (Port == GPIOE) return EXTI_PortSourceGPIOE;
    else if (Port == GPIOF) return EXTI_PortSourceGPIOF;
    else                    return 0;
}

uint8_t FIND_EXTI_PIN_SOURCE(uint32_t Pin)
{
    if (Pin == GPIO_Pin_0)          return EXTI_PinSource0;
    else if (Pin == GPIO_Pin_1)     return EXTI_PinSource1;
    else if (Pin == GPIO_Pin_1)     return EXTI_PinSource1;
    else if (Pin == GPIO_Pin_2)     return EXTI_PinSource2;
    else if (Pin == GPIO_Pin_3)     return EXTI_PinSource3;
    else if (Pin == GPIO_Pin_4)     return EXTI_PinSource4;
    else if (Pin == GPIO_Pin_5)     return EXTI_PinSource5;
    else if (Pin == GPIO_Pin_6)     return EXTI_PinSource6;
    else if (Pin == GPIO_Pin_7)     return EXTI_PinSource7;
    else if (Pin == GPIO_Pin_8)     return EXTI_PinSource8;
    else if (Pin == GPIO_Pin_9)     return EXTI_PinSource9;
    else if (Pin == GPIO_Pin_10)    return EXTI_PinSource10;
    else if (Pin == GPIO_Pin_11)    return EXTI_PinSource11;
    else if (Pin == GPIO_Pin_12)    return EXTI_PinSource12;
    else if (Pin == GPIO_Pin_13)    return EXTI_PinSource13;
    else if (Pin == GPIO_Pin_14)    return EXTI_PinSource14;
    else                            return 0;
}

uint32_t FIND_EXTI_LINE(uint32_t Pin)
{
    if (Pin == GPIO_Pin_0)          return EXTI_Line0;
    else if (Pin == GPIO_Pin_1)     return EXTI_Line1;
    else if (Pin == GPIO_Pin_2)     return EXTI_Line2;
    else if (Pin == GPIO_Pin_3)     return EXTI_Line3;
    else if (Pin == GPIO_Pin_4)     return EXTI_Line4;
    else if (Pin == GPIO_Pin_5)     return EXTI_Line5;
    else if (Pin == GPIO_Pin_6)     return EXTI_Line6;
    else if (Pin == GPIO_Pin_7)     return EXTI_Line7;
    else if (Pin == GPIO_Pin_8)     return EXTI_Line8;
    else if (Pin == GPIO_Pin_9)     return EXTI_Line9;
    else if (Pin == GPIO_Pin_10)    return EXTI_Line10;
    else if (Pin == GPIO_Pin_11)    return EXTI_Line11;
    else if (Pin == GPIO_Pin_12)    return EXTI_Line12;
    else if (Pin == GPIO_Pin_13)    return EXTI_Line13;
    else if (Pin == GPIO_Pin_14)    return EXTI_Line14;
    else if (Pin == GPIO_Pin_15)    return EXTI_Line15;
    else                            return 0;
}

uint32_t FIND_EXTI_HANDLER(uint32_t Pin)
{
    if      (Pin == GPIO_Pin_0)     return EXTI0_IRQn;
    else if (Pin == GPIO_Pin_1)     return EXTI1_IRQn;
    else if (Pin == GPIO_Pin_2)     return EXTI2_IRQn;
    else if (Pin == GPIO_Pin_3)     return EXTI3_IRQn;
    else if (Pin == GPIO_Pin_4)     return EXTI4_IRQn;
    else if (Pin == GPIO_Pin_5  ||
             Pin == GPIO_Pin_6  ||
             Pin == GPIO_Pin_7  ||
             Pin == GPIO_Pin_8  ||
             Pin == GPIO_Pin_9)     return EXTI9_5_IRQn;
    else if (Pin == GPIO_Pin_10 ||
             Pin == GPIO_Pin_11 ||
             Pin == GPIO_Pin_12 ||
             Pin == GPIO_Pin_13 ||
             Pin == GPIO_Pin_14 ||
             Pin == GPIO_Pin_15)    return EXTI15_10_IRQn;
    else                            return 0;
}

/*----------------------------------------------------------------*/
/*INIT:                                                           */
/*----------------------------------------------------------------*/
/*Inicializacion de puertos y funciones del sistema:*/
void INIT_ALL(void)
{
    /*Inicio del sistema:*/
    SystemInit();

    /*Inicializacion del TIM3:*/
    INIT_TIM3(freqTIM3);

    /*Inicializacion LCD:*/
    INIT_LCD_2x16(LCD_2X16);

    /*Inicializacion puertos USART:*/
//    INIT_USART_RX_TX(_RX, _rx, _TX, _tx, baudRate);

    /*Inicializacion interrupciones por pulso externo:*/
    INIT_EXTINT(_C1,_c1);
    INIT_EXTINT(_C2,_c2);
    INIT_EXTINT(_F1,_f1);
    INIT_EXTINT(_F2,_f2);
}
