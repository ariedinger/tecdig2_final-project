#+TITLE: Proyecto final Técnicas Digitales II
#+AUTHOR: A. Riedinger & G. Garcia

* ÍNDICE :toc:
- [[#sobre-el-proyecto][SOBRE EL PROYECTO]]

* SOBRE EL PROYECTO

El objetivo primordial de este proyecto es diseñar y construir una base lógica digital inicial, con el propósito de validar su funcionalidad y viabilidad técnica. Para lograr esto, se ha dividido el proyecto en dos sistemas interdependientes, pero que operan bajo la supervisión de microcontroladores independientes.

 + El primer sistema, conocido como SATDAC (Sistema de Adquisición y Transmisión de Datos Autosuficiente y Controlable), tiene la capacidad de adquirir datos de temperatura, controlar y desplegar periféricos mediante comandos, y recibir y transmitir datos a través del protocolo UART.

 + El segundo sistema, llamado ERDYTC (Estación de Recepción de Datos y Transmisión de Comandos), está equipado con una pantalla LCD para mostrar los datos recibidos y estados, una interfaz de usuario para ingresar comandos y transmitirlos, almacenamiento en memoria externa, y la capacidad de recibir y transmitir datos mediante UART.

En resumen, este proyecto tiene como objetivo crear una base lógica digital inicial para validar la funcionalidad y viabilidad técnica de los sistemas SATDAC y ERDYTC, los cuales son interdependientes y operan bajo la supervisión de microcontroladores independientes. Con su amplia gama de capacidades, estos sistemas demuestran un gran potencial para aplicaciones futuras en el campo de la adquisición de datos y la transmisión de comandos.
