/* PWM para servo motores: 20ms de periodo
 * IMPORTANTE: HSE_VALUE = 8000000 y PLL_M = 8*/

#include "stm32f4xx.h"

TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
TIM_OCInitTypeDef  TIM_OCInitStructure;
uint16_t pos_servo;
uint16_t PrescalerValue = 0;


GPIO_InitTypeDef  GPIO_InitStructure;

void TIM_Config(void);
void servo_write1(int angulo);
int map(int st1, int fn1, int st2, int fn2, int valor);
void PWM_Config();

int main(void)
{
	int i = 0;
	SystemInit();
	TIM_Config();	//Configura los pines y sus clocks.
	PWM_Config();


	while (1)
	{
		servo_write1(90);
	}
}

void servo_write1(int angulo)	/*Permite ingresar el angulo a donde se quiere que mueva el servo.*/
{
	pos_servo = map(0, 180, 500, 2500, angulo);
}

int map(int st1, int fn1, int st2, int fn2, int valor)	/*Convierte los valores de cuenta del servo a angulo.*/
{
	return (1.0*(valor-st1))/((fn1-st1)*1.0) * (fn2-st2)+st2;
}

void PWM_Config()
{
	PrescalerValue = 83;	//Señal de T=50ms

	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 20000;		// 1MHz / 50Hz = 20000
	TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	/* PWM1 Mode configuration: Channel1 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = pos_servo;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

	TIM_OC1Init(TIM3, &TIM_OCInitStructure);

	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);

	TIM_OC2Init(TIM3, &TIM_OCInitStructure);

	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);

	/*-----------------------*/

	TIM_ARRPreloadConfig(TIM3, ENABLE);

	/* TIM3 enable counter */
	TIM_Cmd(TIM3, ENABLE);
}

void TIM_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	/* GPIOC clock enable */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

	/* GPIOC Configuration: TIM3 CH1 (PC6), TIM3 CH2 (PC7), TIM3 CH3 (PC8) and TIM3 CH4 (PC9) */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* Connect TIM3 pins to AF2 */
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_TIM3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_TIM3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_TIM3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource9, GPIO_AF_TIM3);
}

