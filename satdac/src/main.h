/*
 * main.h
 *
 *  Created on: 27 jul. 2022
 *      Author: gerog
 */

#ifndef MAIN_H_
#define MAIN_H_

/*----------------------------------------------------------------*/
/*LIBRERIAS:                                                      */
/*----------------------------------------------------------------*/
/*STANDARD:*/
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "math.h"
#include "misc.h"
/*SD:*/
#include "defines.h"
#include "stm32f4xx.h"
#include "tm_stm32f4_delay/tm_stm32f4_delay.h"
#include "tm_stm32f4_fatfs/tm_stm32f4_fatfs.h"
#include <stdio.h>
#include <string.h>
/*USART:*/
#include "stm32f4xx_syscfg.h"
/*TIMERS:*/
#include "stm32f4xx_tim.h"

/*----------------------------------------------------------------*/
/*DECLARACION DE ESTRUCTURAS:                                     */
/*----------------------------------------------------------------*/
TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
TIM_OCInitTypeDef       TIM_OCInitStructure;
EXTI_InitTypeDef        EXTI_InitStructure;
ADC_InitTypeDef         ADC_InitStructure;
ADC_CommonInitTypeDef   ADC_CommonInitStructure;
DMA_InitTypeDef         DMA_InitStructure;
NVIC_InitTypeDef        NVIC_InitStructure;
GPIO_InitTypeDef        GPIO_InitStructure;

/*----------------------------------------------------------------*/
/*DEFINES:                                                        */
/*----------------------------------------------------------------*/

/* * * * * * * * * * *      USART     * * * * * * * * * * * * * * */
/*Baud Rate:*/
#define baudRate 50

/* * * * * * * * * * *      HARDWARE  * * * * * * * * * * * * * * */
/*Servomotor:*/
#define _Servo     GPIOF
#define _servo     GPIO_Pin_9
#define __servo    GPIO_PinSource9

/*Sensor temperatura:*/
#define _LM35   GPIOC
#define _lm35   GPIO_Pin_0

/*RX USART:*/
#define _RX GPIOA
#define _rx GPIO_Pin_3

/*TX USART:*/
/*Conector del medio en la placa - al lado de JP1 en el esquematico*/
/*Lo tiramos con un cablesito porque no esta en el conector que usamos*/
/*El jumper JP1 queda siempre abierto entonces*/
#define _TX GPIOA
#define _tx GPIO_Pin_2

/*Filas del promediador movil FIFO:*/
#define row			 50

/*Promediador movil FIFO con 10 elementos:*/
float lcdFifoBuff[row];
uint8_t i = 0;

/*Variable del arreglo FIFO:*/
float tempSuma = 0.0f;

/*----------------------------------------------------------------*/
/*VARIABLES:                                                      */
/*----------------------------------------------------------------*/
/* * * * * * * * * * *      ADC       * * * * * * * * * * * * * * */
/*Variable para almacenar el valor digital de temperatura:*/
uint32_t tempDig = 0;
/*Valor promedio de temperatura:*/
float    tempAna = 99.9;
/*Flag de que hay nuevos datos para procesar:*/
uint8_t  f_newTempData = 0;

/* * * * * * * * * * *      SERVO     * * * * * * * * * * * * * * */
/*Elementos para mover el servo*/
uint8_t  DE = 3;
uint8_t  elements = 100;

/* * * * * * * * * * *      USART     * * * * * * * * * * * * * * */
/*Recepcion de caracteres:*/
char receivedCode[8];

/* * * * * * * * * * *      TIMERS    * * * * * * * * * * * * * * */
/*Cada cuanto se interrumpe el timer en frecuencia en Hz:*/
uint32_t freq = 5000;

/* * * * * * * * * * *      LOCAL     * * * * * * * * * * * * * * */
/*Variable para almacenar temperatura:*/
char buffTemp[2];

/* * * * * * * * * * *      SD        * * * * * * * * * * * * * * */
uint16_t leds[3]={GPIO_Pin_0, GPIO_Pin_7, GPIO_Pin_14 };
/*Variable para almacenar el valor de temperatura*/
char buffSD[8];
//Fatfs object
FATFS FatFs;
//File object
FIL fil;


/*----------------------------------------------------------------*/
/*DECLARACION FUNCIONES:                                          */
/*----------------------------------------------------------------*/
/* * * * * * * * * * *      ADC       * * * * * * * * * * * * * * */
/*Funcion para procesar los datos del ADC:*/
void READ_TEMP(void);
/*Inicializacion ADC:*/
void INIT_ADC(GPIO_TypeDef* Port, uint16_t Pin);
/*Leer valor ADC:*/
int32_t READ_ADC(GPIO_TypeDef* Port, uint16_t Pin);
/*Control del ADC:*/
ADC_TypeDef *FIND_ADC_TYPE(GPIO_TypeDef *Port, uint32_t Pin);
uint32_t FIND_RCC_APB(ADC_TypeDef *ADCX);
uint8_t FIND_CHANNEL(GPIO_TypeDef *Port, uint32_t Pin);

/* * * * * * * * * * *      SD        * * * * * * * * * * * * * * */
//void WRITE_SD();
void DECODE_SD(float tempFloat);
void LEDS_INIT();
void turnOnlyRed();
void turnOnlyBlue();
void turnOnlyGreen();

/* * * * * * * * * * *      INIT      * * * * * * * * * * * * * * */
/*Inicializacion de todos los parametros del sistema:*/
void INIT_ALL(void);

/* * * * * * * * * * *      USART     * * * * * * * * * * * * * * */
void INIT_USART_RX_TX(GPIO_TypeDef*, uint16_t, GPIO_TypeDef*, uint16_t, uint32_t);

/* * * * * * * * * * *      TIMERS    * * * * * * * * * * * * * * */
void INIT_TIM3(uint32_t Freq);
void INIT_TIM2(uint32_t Freq);

/* * * * * * * * * * *      LOCAL     * * * * * * * * * * * * * * */
void READ_TEMP(void);
void WRITE_SD(void);
char DECODE_TEMP(float tempFloat);

/* * * * * * * * * * *      STANDARD  * * * * * * * * * * * * * * */
void INIT_DO(GPIO_TypeDef* Port, uint32_t Pin);
uint32_t FIND_CLOCK(GPIO_TypeDef *Port);
uint8_t FIND_PINSOURCE(uint32_t Pin);


#endif /* MAIN_H_ */
