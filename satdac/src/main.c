#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "string.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stdio.h"
#include "math.h"
#include "misc.h"
#include "tm_stm32f4_servo.h"

/*------------------------------------------------------------------------------
DECLARACION DE ESTRUC//	for(uint8_t i=0;i<RCV_BUFFER_SIZE;i++)
//	{
//		rcvMessage[i]=0x00;
//	}TURAS:
------------------------------------------------------------------------------*/
TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
TIM_OCInitTypeDef       TIM_OCInitStructure;
EXTI_InitTypeDef        EXTI_InitStructure;
ADC_InitTypeDef         ADC_InitStructure;
ADC_CommonInitTypeDef   ADC_CommonInitStructure;
DMA_InitTypeDef         DMA_InitStructure;
NVIC_InitTypeDef        NVIC_InitStructure;
GPIO_InitTypeDef        GPIO_InitStructure;

/*------------------------------------------------------------------------------
LOCAL:
------------------------------------------------------------------------------*/
char c[1000];
void ACK(void);

#define TM_USART2_BUFFER_SIZE 23
#define RCV_BUFFER_SIZE 10
static char rcvBuffer[TM_USART2_BUFFER_SIZE];
static uint16_t rcvMessage[TM_USART2_BUFFER_SIZE];
static uint32_t rcvIndex = 0;
static uint8_t rcvMessageSize = 0;

/*------------------------------------------------------------------------------
SD:
------------------------------------------------------------------------------*/
#include "defines.h"
#include "stm32f4xx.h"
#include "tm_stm32f4_delay/tm_stm32f4_delay.h"
#include "tm_stm32f4_fatfs/tm_stm32f4_fatfs.h"
#include <stdio.h>
#include <string.h>

/* * * * * * * * * * *      SD        * * * * * * * * * * * * * * */
uint16_t leds[3]={GPIO_Pin_0, GPIO_Pin_7, GPIO_Pin_14 };
/*Variable para almacenar el valor de temperatura*/
char buffSD[10];
/*Contador para parar el guardado de datos en la SD:*/
uint8_t contSD = 0;
float tempSD[10];
//Fatfs object
FATFS FatFs;
//File object
FIL fil;

void LEDS_INIT();
void turnOnlyRed();
void turnOnlyBlue();
void turnOnlyGreen();
void WRITE_SD();
void STOP_SD();
void TIM3_IRQHandler(void);

void GPIO_Configuration(void) ;
void USART2_Configuration(void);
void CE_send_BT(char *content);
uint8_t CE_read_word_BT( char *buffer, int tam);

//para leer


/*------------------------------------------------------------------------------
 ADC:
 ------------------------------------------------------------------------------*/
/*Sensor temperatura:*/
#define _LM35   GPIOC
#define _lm35   GPIO_Pin_0

/* * * * * * * * * * *         LIBRERIAS       * * * * * * * * * * * * * * */
#include "stm32f4xx_adc.h"

/* * * * * * * * * * *         VARIABLES       * * * * * * * * * * * * * * */
/*Variable para almacenar el valor digital de temperatura:*/
uint32_t tempDig = 0;
/*Valor promedio de temperatura:*/
float tempAna = 0.0;
/*Arreglo FIFO:*/
float tempFifo[10];
/*Variable para recorrer el arreglo FIFO:*/
uint8_t queue = 0;
/*Sumatoria de todas los valores FIFO:*/
float tempSum = 0.0;
/*Contador para enviar un nuevo dato de temperatura:*/
uint32_t tempCont = 0;
/*Convertir el valor de temperatura de flotante a char:*/
char tempBuff[10];

/* * * * * * * * * * *         FUNCIONES       * * * * * * * * * * * * * * */
/*Actualizacion de temperatura, enviar un nuevo dato:*/
void UPDATE_TEMP(void);
/*Inicializacion ADC:*/
void INIT_ADC(GPIO_TypeDef* Port, uint16_t Pin);
/*Leer valor ADC:*/
int32_t READ_ADC(GPIO_TypeDef* Port, uint16_t Pin);
/*Control del ADC:*/
ADC_TypeDef *FIND_ADC_TYPE(GPIO_TypeDef *Port, uint32_t Pin);
uint32_t FIND_RCC_APB(ADC_TypeDef *ADCX);
uint8_t FIND_CHANNEL(GPIO_TypeDef *Port, uint32_t Pin);
void INIT_DO(GPIO_TypeDef *Port, uint32_t Pin);
uint32_t FIND_CLOCK(GPIO_TypeDef* Port);
uint8_t FIND_PINSOURCE(uint32_t Pin);

/*------------------------------------------------------------------------------
 SERVO:
 ------------------------------------------------------------------------------*/
/* Servo structs */
TM_SERVO_t Servo1;

/*------------------------------------------------------------------------------
 TIMERS:
 ------------------------------------------------------------------------------*/
/* * * * * * * * * * *         LIBRERIAS       * * * * * * * * * * * * * * */
#include "stm32f4xx_tim.h"

/* * * * * * * * * * *         FUNCIONES       * * * * * * * * * * * * * * */
void INIT_TIM3(uint32_t Freq);
void INIT_TIM2(uint32_t Freq);

/* * * * * * * * * * *         VARIABLES       * * * * * * * * * * * * * * */
/*Cada cuanto se interrumpe el timer en frecuencia en Hz:*/
uint32_t freq = 5000;

/*------------------------------------------------------------------------------
 MAIN:
 ------------------------------------------------------------------------------*/
int main(void) {

	SystemInit();

	GPIO_Configuration();
	USART2_Configuration();

	/*Inicialización del TIM3 a 5 kHz:*/
	INIT_TIM3(freq);
	/*Inicializacion PC3 como ADC (LM35)*/
	INIT_ADC(_LM35, _lm35);

	/* Initialize servo 1, TIM2, Channel 1, Pinspack 2 = PA5 */
	TM_SERVO_Init(&Servo1, TIM2, TM_PWM_Channel_1, TM_PWM_PinsPack_2);

	LEDS_INIT();
	TM_DELAY_Init();

	TM_SERVO_SetDegrees(&Servo1, 0);

	UPDATE_TEMP();

	while (1)
	{
	}
}

/*------------------------------------------------------------------------------
 INTERRUPCIONES:
 ------------------------------------------------------------------------------*/
void USART1_IRQHandler(void)
{
	if(USART_GetITStatus(USART1,USART_IT_RXNE)==SET)
	{
		if(USART_GetFlagStatus(USART1,USART_IT_RXNE)==SET)
		{
			rcvBuffer[rcvIndex]=(char) USART_ReceiveData(USART1);

			//Buffer full or end of current message
			if(rcvIndex+1>=RCV_BUFFER_SIZE || rcvBuffer[rcvIndex]=='\n')
			{
				//Placing message in message buffer - setting flag

				uint32_t i;
				for(i=0;i<RCV_BUFFER_SIZE;i++)
				{
					if(i<=rcvIndex)
					{
						rcvMessage[i]=rcvBuffer[i];
					}
					else
					{
						rcvMessage[i]=0x00;
					}
				}

//				CE_read_word_BT(c,10);

				//Clear receive buffer
				rcvMessageSize=rcvIndex;
				//processMessageRecieved();

				if (!strcmp(rcvMessage, "a"))
					WRITE_SD();
				else if (!strcmp(rcvMessage, "b"))
					TM_SERVO_SetDegrees(&Servo1, 0);
				else if (!strcmp(rcvMessage, "c"))
					TM_SERVO_SetDegrees(&Servo1, 180);
				else
					rcvMessage[0]=0x00;

				for(i=0;i<RCV_BUFFER_SIZE;i++)
				{
					rcvMessage[i]=0x00;
				}

				for(i=0;i<RCV_BUFFER_SIZE;i++)
				{
					rcvBuffer[i]=0x00;
				}


				rcvIndex=0;
			}
			else
			{
				rcvIndex++;
			}
		}
		USART_ClearITPendingBit(USART1,USART_IT_RXNE);
	}
}

/*Interrupcion por vencimiento de cuenta de TIM3 cada 1/FS:*/
void TIM3_IRQHandler(void) {

	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) {
		/*Actualizar el contador de temperatura:*/
		if (tempCont >= 5e3)
		{
			tempCont = 0;
//		    UPDATE_TEMP();
		}
		else tempCont++;

		/*Rehabilitacion del timer:*/
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	}
}

/*----------------------------------------------------------------*/
/*                     SD:                                        */
/*----------------------------------------------------------------*/
void STOP_SD()
{
	f_close(&fil);

	//Unmount drive, don't forget this!
	f_mount(0, "", 1);

	contSD = 1;
}

void WRITE_SD()
{
	//Free and total space
	uint32_t total, free;

	//Mount drive
	if (f_mount(&FatFs, "", 1) == FR_OK) {
		//Mounted OK, turn on RED LED
		turnOnlyRed();

		//Try to open file
		if (f_open(&fil, "log.txt", FA_OPEN_ALWAYS | FA_READ | FA_WRITE)
				== FR_OK) {
			//File opened, turn off RED andWRITE_SD turn on GREEN led
			turnOnlyGreen();

			for (uint8_t i = 0; i < 5; i++)
			{
				sprintf(buffSD, "%.1f,", tempSD[i]);

				//If we put more than 0 characters (everything OK)
				if (f_puts(buffSD, &fil) > 0) {
					if (TM_FATFS_DriveSize(&total, &free) == FR_OK) {
						//Data for drive size are valid
					}
					//Turn on both leds
					turnOnlyBlue();
				}
			}
			f_close(&fil);;
		}
		for(uint8_t i=0;i<10;i++)
		{
			tempSD[i]=0x00;
		}

		//Unmount drive, don't forget this!
		f_mount(0, "", 1);
	}
}

/*----------------------------------------------------------------
 ADC:
 ----------------------------------------------------------------*/
/*Actualizar temperatura, enviar un nuevo dato:*/
void UPDATE_TEMP() {
	/*Almacenar el valor digital de temperatura:*/
	tempDig = READ_ADC(_LM35, _lm35);

	tempAna = (float) ((tempDig * 1.95 / 4095 ) * (1/10e-3));

	tempFifo[queue] = tempAna;

	if (queue < 10) queue++;
	else
	{
		for(uint8_t i=0;i<10;i++)
		{
			tempFifo[i]=0x00;
		}
		queue = 0;
	}

	for(uint8_t i = 0; i < queue ; i++)
		tempSum += tempFifo[i];

	if (queue != 0) sprintf(tempBuff, "%.1f\n", tempSum/queue);
//	else sprintf(tempBuff, "%.1f\n", 33.3);
	CE_send_BT(tempBuff);

	tempSD[contSD] = tempSum;

	if (contSD < 10) contSD++;
	else contSD = 0;

	tempSum = 0.0f;

}

/*----------------------------------------------------------------*/
/*TIMERS:                                                         */
/*----------------------------------------------------------------*/
/* * * * * * * * * * *         FUNCIONES       * * * * * * * * * * * * * * */
/*Inicializacion del TIM3:*/
void INIT_TIM3(uint32_t Freq)
{

    /*Habilitacion del clock para el TIM3:*/
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

    /*Habilitacion de la interrupcion por agotamiento de cuenta del TIM3:*/
    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /*Actualización de los valores del TIM3:*/
    SystemCoreClockUpdate();
    TIM_ITConfig(TIM3, TIM_IT_Update, DISABLE);
    TIM_Cmd(TIM3, DISABLE);

    /*Definicion de la base de tiempo:*/
    uint32_t TimeBase = 200e3;

    /*Computar el valor del preescaler en base a la base de tiempo:*/
    uint16_t PrescalerValue = 0;
    PrescalerValue = (uint16_t) ((SystemCoreClock / 2) / TimeBase) - 1;

    /*Configuracion del tiempo de trabajo en base a la frecuencia:*/
    TIM_TimeBaseStructure.TIM_Period = TimeBase / Freq - 1;
    TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

    /*Habilitacion de la interrupcion:*/
    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

    /*Habilitacion del contador:*/
    TIM_Cmd(TIM3, ENABLE);
}

/*----------------------------------------------------------------*/
/*ADC CONFIG:                                                     */
/*----------------------------------------------------------------*/
/*Inicializacion del ADC:*/
void INIT_ADC(GPIO_TypeDef* Port, uint16_t Pin) {
	uint32_t Clock;
	Clock = FIND_CLOCK(Port);

	ADC_TypeDef* ADCX;
	ADCX = FIND_ADC_TYPE(Port, Pin);

	uint32_t RCC_APB;
	RCC_APB = FIND_RCC_APB(ADCX);

	uint8_t Channel;
	Channel = FIND_CHANNEL(Port, Pin);

	GPIO_InitTypeDef GPIO_InitStructure;
	ADC_InitTypeDef ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;

	//Habilitacion del Clock para el puerto donde esta conectado el ADC:
	RCC_AHB1PeriphClockCmd(Clock, ENABLE);

	//Configuracion del PIN del ADC como entrada ANALOGICA.
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(Port, &GPIO_InitStructure);

	//Activar ADC:
	RCC_APB2PeriphClockCmd(RCC_APB, ENABLE);

	//ADC Common Init:
	ADC_CommonStructInit(&ADC_CommonInitStructure);
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div4; // max 36 MHz segun datasheet
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

	//ADC Init:
	ADC_StructInit(&ADC_InitStructure);
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 1;
	ADC_Init(ADCX, &ADC_InitStructure);

	//Establecer la configuración de conversion:
	ADC_InjectedSequencerLengthConfig(ADCX, 1);
	ADC_SetInjectedOffset(ADCX, ADC_InjectedChannel_1, 0);
	ADC_InjectedChannelConfig(ADCX, Channel, 1, ADC_SampleTime_480Cycles);

	/* Poner en marcha ADC ----------------------------------------------------*/
	ADC_Cmd(ADCX, ENABLE);
}

/*Leer valor de ADC:*/
int32_t READ_ADC(GPIO_TypeDef* Port, uint16_t Pin) {
	uint32_t ADC_DATA;

	ADC_TypeDef* ADCX;
	ADCX = FIND_ADC_TYPE(Port, Pin);

	ADC_ClearFlag(ADCX, ADC_FLAG_JEOC);
	ADC_SoftwareStartInjectedConv(ADCX);
	while (ADC_GetFlagStatus(ADCX, ADC_FLAG_JEOC) == RESET);

	ADC_DATA = ADC_GetInjectedConversionValue(ADCX, ADC_InjectedChannel_1);
	return ADC_DATA;
}

/*Control del ADC:*/
ADC_TypeDef* FIND_ADC_TYPE(GPIO_TypeDef* Port, uint32_t Pin) {
	ADC_TypeDef* ADCX;

	if ((Port == GPIOA
			&& (Pin == GPIO_Pin_0 || Pin == GPIO_Pin_1 || Pin == GPIO_Pin_2
					|| Pin == GPIO_Pin_3 || Pin == GPIO_Pin_4
					|| Pin == GPIO_Pin_5 || Pin == GPIO_Pin_6
					|| Pin == GPIO_Pin_7)) ||

	(Port == GPIOB && (Pin == GPIO_Pin_0 || Pin == GPIO_Pin_1))
			||

			(Port == GPIOC
					&& (Pin == GPIO_Pin_0 || Pin == GPIO_Pin_1
							|| Pin == GPIO_Pin_2 || Pin == GPIO_Pin_3
							|| Pin == GPIO_Pin_4 || Pin == GPIO_Pin_5)))
		ADCX = ADC1;

	else if ((Port == GPIOF
			&& (Pin == GPIO_Pin_3 || Pin == GPIO_Pin_4 || Pin == GPIO_Pin_5
					|| Pin == GPIO_Pin_6 || Pin == GPIO_Pin_7
					|| Pin == GPIO_Pin_8 || Pin == GPIO_Pin_9
					|| Pin == GPIO_Pin_10)))
		ADCX = ADC3;

	else
		ADCX = NULL;

	return ADCX;
}

uint32_t FIND_RCC_APB(ADC_TypeDef* ADCX) {
	uint32_t RCC_APB;

	if (ADCX == ADC1)
		RCC_APB = RCC_APB2Periph_ADC1;
	else if (ADCX == ADC3)
		RCC_APB = RCC_APB2Periph_ADC3;
	else
		RCC_APB = 0;

	return RCC_APB;
}

uint8_t FIND_CHANNEL(GPIO_TypeDef* Port, uint32_t Pin) {
	uint8_t Channel;

	if (Port == GPIOA && Pin == GPIO_Pin_0)
		Channel = ADC_Channel_0;
	else if (Port == GPIOA && Pin == GPIO_Pin_1)
		Channel = ADC_Channel_1;
	else if (Port == GPIOA && Pin == GPIO_Pin_2)
		Channel = ADC_Channel_2;
	else if (Port == GPIOA && Pin == GPIO_Pin_3)
		Channel = ADC_Channel_3;
	else if (Port == GPIOA && Pin == GPIO_Pin_4)
		Channel = ADC_Channel_4;
	else if (Port == GPIOA && Pin == GPIO_Pin_5)
		Channel = ADC_Channel_5;
	else if (Port == GPIOA && Pin == GPIO_Pin_6)
		Channel = ADC_Channel_6;
	else if (Port == GPIOA && Pin == GPIO_Pin_7)
		Channel = ADC_Channel_7;
	else if (Port == GPIOB && Pin == GPIO_Pin_0)
		Channel = ADC_Channel_8;
	else if (Port == GPIOB && Pin == GPIO_Pin_1)
		Channel = ADC_Channel_9;
	else if (Port == GPIOC && Pin == GPIO_Pin_0)
		Channel = ADC_Channel_10;
	else if (Port == GPIOC && Pin == GPIO_Pin_1)
		Channel = ADC_Channel_11;
	else if (Port == GPIOC && Pin == GPIO_Pin_2)
		Channel = ADC_Channel_12;
	else if (Port == GPIOC && Pin == GPIO_Pin_3)
		Channel = ADC_Channel_13;
	else if (Port == GPIOC && Pin == GPIO_Pin_4)
		Channel = ADC_Channel_14;
	else if (Port == GPIOC && Pin == GPIO_Pin_5)
		Channel = ADC_Channel_15;
	else if (Port == GPIOF && Pin == GPIO_Pin_3)
		Channel = ADC_Channel_9;
	else if (Port == GPIOF && Pin == GPIO_Pin_4)
		Channel = ADC_Channel_14;
	else if (Port == GPIOF && Pin == GPIO_Pin_5)
		Channel = ADC_Channel_15;
	else if (Port == GPIOF && Pin == GPIO_Pin_6)
		Channel = ADC_Channel_4;
	else if (Port == GPIOF && Pin == GPIO_Pin_7)
		Channel = ADC_Channel_5;
	else if (Port == GPIOF && Pin == GPIO_Pin_8)
		Channel = ADC_Channel_6;
	else if (Port == GPIOF && Pin == GPIO_Pin_9)
		Channel = ADC_Channel_7;
	else if (Port == GPIOF && Pin == GPIO_Pin_10)
		Channel = ADC_Channel_8;
	else
		Channel = 0;

	return Channel;
}

/*Inicializacion de salida digital:*/
void INIT_DO(GPIO_TypeDef *Port, uint32_t Pin) {
  // Estructura de configuracion
  GPIO_InitTypeDef GPIO_InitStructure;

  // Habilitacion de la senal de reloj para el periferico:
  uint32_t Clock;
  Clock = FIND_CLOCK(Port);
  RCC_AHB1PeriphClockCmd(Clock, ENABLE);

  // Se configura el pin como entrada (GPI0_MODE_IN):
  GPIO_InitStructure.GPIO_Pin = Pin;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

  // Se aplica la configuracion definida anteriormente al puerto:
  GPIO_Init(Port, &GPIO_InitStructure);
}

/*Encontrar el clock de un determinado puerto:*/
uint32_t FIND_CLOCK(GPIO_TypeDef* Port)
{
    uint32_t Clock;

    if      (Port == GPIOA) Clock = RCC_AHB1Periph_GPIOA;
    else if (Port == GPIOB) Clock = RCC_AHB1Periph_GPIOB;
    else if (Port == GPIOC) Clock = RCC_AHB1Periph_GPIOC;
    else if (Port == GPIOD) Clock = RCC_AHB1Periph_GPIOD;
    else if (Port == GPIOE) Clock = RCC_AHB1Periph_GPIOE;
    else if (Port == GPIOF) Clock = RCC_AHB1Periph_GPIOF;
    else if (Port == GPIOG) Clock = RCC_AHB1Periph_GPIOG;
    return Clock;
}

/*Encontrar el PinSource de un determinado Pin:*/
uint8_t FIND_PINSOURCE(uint32_t Pin)
{
    if     (Pin == GPIO_Pin_0)  return GPIO_PinSource0;
    else if(Pin == GPIO_Pin_1)  return GPIO_PinSource1;
    else if(Pin == GPIO_Pin_2)  return GPIO_PinSource2;
    else if(Pin == GPIO_Pin_3)  return GPIO_PinSource3;
    else if(Pin == GPIO_Pin_4)  return GPIO_PinSource4;
    else if(Pin == GPIO_Pin_5)  return GPIO_PinSource5;
    else if(Pin == GPIO_Pin_6)  return GPIO_PinSource6;
    else if(Pin == GPIO_Pin_7)  return GPIO_PinSource7;
    else if(Pin == GPIO_Pin_8)  return GPIO_PinSource8;
    else if(Pin == GPIO_Pin_9)  return GPIO_PinSource9;
    else if(Pin == GPIO_Pin_10) return GPIO_PinSource10;
    else if(Pin == GPIO_Pin_11) return GPIO_PinSource11;
    else if(Pin == GPIO_Pin_12) return GPIO_PinSource12;
    else if(Pin == GPIO_Pin_13) return GPIO_PinSource13;
    else if(Pin == GPIO_Pin_14) return GPIO_PinSource14;
    else if(Pin == GPIO_Pin_15) return GPIO_PinSource15;
    else
         return 0;
}


void turnOnlyRed() {
	//LED Rojo
	GPIO_SetBits(GPIOB, leds[2]);
	GPIO_ResetBits(GPIOB, leds[0]);
	GPIO_ResetBits(GPIOB, leds[1]);
}
void turnOnlyBlue() {
	/* Distance between 20 and 50cm */
	//LED Azul
	GPIO_SetBits(GPIOB, leds[1]);
	GPIO_ResetBits(GPIOB, leds[0]);
	GPIO_ResetBits(GPIOB, leds[2]);
}
void turnOnlyGreen() {
	/* Distance more than 50cm */
	//LED Verde
	GPIO_ResetBits(GPIOB, leds[1]);
	GPIO_ResetBits(GPIOB, leds[2]);
	GPIO_SetBits(GPIOB, leds[0]);
}

void LEDS_INIT() {

	GPIO_InitTypeDef GPIO_InitStructure; //Estructura para la inicialización de los GPIO

	//Habilitación de la señal de reloj para el periferico GPIOB

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	//Se configuran los pines PB0, PB14 y PB7

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_7 | GPIO_Pin_14;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

	GPIO_Init(GPIOB, &GPIO_InitStructure); //Se aplica la configuración definida anteriormente al puerto B
}


/*------------------------------------------------------------------------------
USART:
------------------------------------------------------------------------------*/
void GPIO_Configuration(void) {
	GPIO_InitTypeDef GPIO_InitStruct;

	// Enable clock for GPIOA
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE); //verrr

	/**
	 * Tell pins PA9 and PA10 which alternating function you will use
	 * @important Make sure, these lines are before pins configuration!
	 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
	// Initialize pins as alternating function
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

}

void USART2_Configuration(void) {
	USART_InitTypeDef USART_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;

	/**
	 * Enable clock for USART1 peripheral
	 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	/**
	 * Set Baudrate to value you pass to function
	 * Disable Hardware Flow control
	 * Set Mode To TX and RX, so USART will work in full-duplex mode
	 * Disable parity bit
	 * Set 1 stop bit
	 * Set Data bits to 8
	 *
	 * Initialize USART1
	 * Activate USART1
	 */
	USART_InitStruct.USART_BaudRate = 9600;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART1, &USART_InitStruct);
	USART_Cmd(USART1, ENABLE);

	/**
	 * Enable RX interrupt
	 */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

	/**
	 * Set Channel to USART1
	 * Set Channel Cmd to enable. That will enable USART1 channel in NVIC
	 * Set Both priorities to 0. This means high priority
	 *
	 * Initialize NVIC
	 */
	NVIC_InitStruct.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 2;
	NVIC_Init(&NVIC_InitStruct);

}

uint8_t CE_read_word_BT( char *buffer, int tam) {

	static int pos = 0;

	//if (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET) {
		char caracter = USART_ReceiveData(USART1);

		if (caracter == '\n' || caracter == '\r') {
			pos = 0;
			return 0;
		}
		else {
			buffer[pos] = caracter;
			pos++;
		}

		if (pos > tam-1) {
			pos = 0;
			return 0;
		}
		return 1;

	//}

	return 1;
}
//para escribir
void CE_send_BT(char *content) {

	int i;
	for (i = 0; i < strlen(content); i++) {

		char dato = content[i];
		USART1->DR = dato;
		//USART_SendData(USART1,dato);
		while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
			;
	}
}
